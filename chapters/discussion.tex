% !TEX root = ../Documentation_Bern_RTOS_PAI.tex

\chapter{Bern RTOS Concept}
\label{chap:discussion}

Having seen what features RTOS typically offer, how \ucos-III implements a kernel, how Zephyr is structured and what characteristics Rust has, we can now discuss the concept for the implementation of the Bern RTOS.

%-------------------------------------------------------------------------------
\section{Security \& Functional Safety}

A major threat for embedded systems are network interfaces, because they can put an unanticipated load on the system. For example, a faulty device could increase traffic on the network or send wrongly packed data. IoT devices are especially vulnerable to this threat as they are accessible through the internet. But with increased network connectivity this concern also applies to industrial systems, wireless sensor networks or building automation. An RTOS should therefore prevent overload or errors from one task to spread to the whole system.

Most of the general purpose RTOS we have seen in the survey offer a SIL-3 certified variant. The certification proves functional safety for automotive and railway applications. Although Rust is stricter at compile time than C, the language is not certified for functional safety applications and no guidelines like MISRA C/C++ are in place. Still, functional safety is a concern and the implementation of the Bern RTOS should rely on the stable Rust compiler version (not beta or nightly). Moreover, the RTOS must be usable without dynamic memory allocation.

The Bern RTOS strives to isolate bugs that can arise from external factors such as network interfaces and software bugs that the compiler cannot detect. As a consequence tasks on ARM architectures should be run in thread (unprivileged) mode and access to the hardware should only be possible through the kernel. Any attempt to access a resource a task has no privileges can be caught by the memory protection unit (MPU) \cite{yiu_beckmann_2014}. As a trade-off this solution will introduce some latency in reaction time because the MPU configuration has to be changed on a context switch and to access hardware the CPU must switch to kernel mode.

Internet of things raised concerns about the security of embedded systems that are connected to internet. This means above all that systems and network stacks must be updated to close security loopholes. A bootloader, encryption and key management is needed for a secure update process. These are features that can added in modules at a later stage, but I will neglect them for the kernel development.

The main objective of the Bern RTOS is a system that does not crash. Strong typing and Rusts ownership will help to reduce the number of bugs at compile-time. The architecture design and memory protection hardware should isolate bugs at runtime. This will help to make the RTOS stable, safe and secure.

%-------------------------------------------------------------------------------
\section{Flexibility \& Scalability}

In the end an RTOS is just a tool. It is supposed to simplify embedded systems development, by moving some responsibilities (e.g. time management, synchronization) from the developer to the RTOS. But when developing an RTOS we cannot anticipate all the applications the user will want to use the system. Thus, the system should be flexible enough to be used for some unforeseen use cases. Nevertheless, the RTOS flexibility will be restricted in some ways in exchange for safety by introducing a resource privilege system.

Flexibility is for example needed in memory management. Most systems will probably get by with static memory management. But for some application dynamic memory management is beneficial or necessary. For these applications the Bern RTOS should provide deterministic pool of memory blocks.

The Bern RTOS should be scalable, so it can run on many architectures regardless of power and memory constraints. This is mainly achieved by abstracting the architecture and avoiding artificial limits (e.g. max number of tasks) in the kernel design. Some applications emphasize one part of an RTOS more than others, i.e. power management is important on wireless sensor node while a motor controller needs timeliness.

%-------------------------------------------------------------------------------
\section{User workflow}

The Devicetree and architecture of Zephyr restricts its user to a specific workflow, where the RTOS handles hardware and services. The advantage of an RTOS that handles the hardware is that it can ensure resource access is safe, either at runtime by applying permissions or to some extent with Rusts ownership at compile time. The problem here is that the learning curve to adopt such a system is steep and most people will probably only use a system that already has widespread hardware support.

Implementing and maintaining drivers for every hardware requires a large community or MCU manufacturer support. The Rust community already has a hardware abstraction project in place, the embedded-hal. The HAL already supports numerous MCUs, especially support for STM32 devices is well advanced. Some companies might still want to implement their own HAL based on memory mapped registers. For the Bern RTOS we need to find a solution that allows the usage of the embedded-hal or a custom HAL. 

The idea for the user workflow is that the Bern RTOS can be adopted to an existing workflow to some extent. In particular should it be possible for a user to reuse existing code (e.g. a custom HAL or embedded-hal based device drivers) when switching from bare-metal to the Bern RTOS.

To manage and configure projects in Zephyr custom tools are provided. At some point the Bern RTOS might reach a level of complexity where such tools are necessary as well, e.g. to create projects, to configure the RTOS or to validate permissions. The goal is again to keep the workflow a user is already familiar with by extending Cargo.


%-------------------------------------------------------------------------------
\section{Target Hardware}

CPUs in computers predominantly implement the x86\_64 architecture. The landscape is different on microcontrollers: there are architectures a manufacturer developed (MSP430, AVR, PIC) and nowadays more common licensed architectures (e.g. ARM Cortex-A/M/R, RISC-V). To run an RTOS kernel on all these cores, architecture specific code must be abstracted. Although the Bern RTOS will not support every architecture, the kernel will be written platform agnostic from the start.

For the Bern RTOS I will focus on ARMv7-M (Cortex-M3) and ARMv7E-M (Cortex-M4) architecture, because they are nearly identical and used in most microcontrollers. If possible, ARMv6-M (Cortex-M0+) will be supported as well.  This core is used for low power systems or as application processor in wireless SoCs. For simulation purposes a POSIX implementation would be nice. 

Some modern microcontrollers have multiple instances of a CPU core (symmetric multiprocessing, SMP) or cores of different architecture (asymmetric multiprocessing, AMP). Multiprocessing has a significant influence on kernel design, but for now I will focus on single core microcontroller.


%-------------------------------------------------------------------------------
\section{Architecture}

\begin{figure}[htb!]
    \centering
    \includegraphics{bern_rtos_architecture.pdf}
    \caption[Bern RTOS architecture draft]{Bern RTOS architecture draft; red: high priority components, blue: low priority components, green: not part of the Bern RTOS}
    \label{fig:bern-rtos-arch}
    \vspace{0.5\baselineskip}
\end{figure}

The architecture of the Bern RTOS in \autoref{fig:bern-rtos-arch} has been drafted based on the requirements above and Zephyrs architecture. The right side shows the intended separation of privileges: Components in \emph{kernel mode} have all privileges and have access to every resource, while components in \emph{thread mode} will run with restricted permissions.

%...............................................................................
\subsection{Hardware Abstraction}

The most basic form of hardware access on a microcontroller are memory mapped registers. In Rust the community develops \emph{peripheral access crates} (PAC) to manage registers. There are two PACs per microcontroller, one for the Core registers and peripherals (e.g. on ARM Cortex-M4F: registers, SysTick timer, Instrumentation Trace Macrocell, MPU) and one for the MCU peripherals (e.g. SPI, UART, timers).

Based on the MCU peripherals is the hardware abstraction layer. Here the memory mapped registers are embedded into functions of a common interface. This layer hides any memory addresses from the user and thus abstracts the hardware.
The Bern RTOS will not implement the HAL, the user will have to provide it. But the system design should allow to either use the embedded-hal or a custom HAL. Allowing for a custom HAL is necessary because a company, that uses one microcontroller for lots of products, might want to implement a more feature rich but less generic HAL.

The intent for low level drivers is a set of support functions that are not yet device drivers, e.g. transmitting a block of memory over a bus. These functions are built on a generic HAL interface and should make device drivers more efficient.

%...............................................................................
\subsection{Kernel}

The kernel of the Bern RTOS consists of a set of modules. Thereby we can start with a minimal prototype and add features gradually.


\subsubsection{Architecture Interface}

The architecture interface defines the functions that have to ported in order for the Bern RTOS to run on another architecture. 


\subsubsection{Scheduler}

Tasks will be scheduled priority based preemptively. For tasks with the same priority the user will be able to choose whether to enable time-slicing as this features adds overhead. In Zephyr, for example, tasks with priority greater than or equal to zero are time-sliced, tasks with negative priority are scheduled cooperatively.


\subsubsection{IPC}

As any OS, mutex, semaphores and message queues are planned for the Bern RTOS. For the mutex the aim is to include a resource within a mutex in the same way the Rust standard library does. For message passing we need to find a solution to efficiently pass data of different sizes. \ucos-III uses memory blocks to pass large data from one task to another without copying. On systems with dynamic memory this might be a solution. 


\subsubsection{Log}

Logging is an integral part of any embedded system. Any module will be able to log events. For efficient debugging the log module needs to provide filtering by event severity and origin. Logs should also support multiple back-ends, because during development a serial port suffices but the end-use application will some need non-volatile storage.


\subsubsection{Resource Manager}

The role of the resource manager is to assure that a task has the permission to access a resource. This also includes resource sharing, i.e. for buses. However, there is yet no model defined on how a task can get permission for a resource.


%...............................................................................
\subsection{Kernel API}

The kernel API separates all components that depend on the hardware from the rest of the system. It consists of several APIs for the individual parts of the kernel, e.g. for the scheduler, for IPC and for the resource manager.

The kernel API forms the barrier between thread and kernel mode. Meaning that the components above the kernel API have limited permissions and cannot propagate a critical error to the kernel. This is a precaution that should reduce kernel panics to a minimum.

%...............................................................................
\subsection{System Services}

System services provide extra functionality to the user but do not have the permissions the kernel has. These service include for example device drivers, file system and network stacks.

Device drivers are meant to be used for peripheral components that are not part of the MCU, e.g. accelerometer, Ethernet PHY or EEPROM. The drivers will mostly be written by the end user or the open-source community. The device drivers have been placed in the thread mode because they quite often contain bugs that can cause a kernel to panic.

The Rust community has already developed network stacks (e.g. smoltcp) and support to can access a file system (e.g. fat32). The goal here is not to redevelop these packages but to wrap them so that they work with the Bern RTOS. For example, Zephyr provides an API that allows the use of multiple file system implementations at the same time.


%-------------------------------------------------------------------------------
\section{Comparison with existing Rust RTOS}

RTIC is a real-time framework, it bridges the gap between bare-metal and RTOS. Because RTIC uses hardware interrupts, it is the most efficient real-time solution with the smallest footprint. The Bern RTOS in contrast, is similar to a classical RTOS by using a preemptive time-sliced scheduler. As the Bern RTOS will have more control over a system than RTIC, it should be possible, for example, to manage power states without user interaction.

Drone OS is probably comes closest to classical RTOS like \ucos-III that exist in Rust. Unlike the Bern RTOS, which tries to use the existing embedded-hal, Drone OS has a custom register model based on CMSIS-SVD files. 

Tock OS uses a micro kernel and is thus the most secure and stable system. This leads to many system calls which worsen real-time performance, but the system was not designed for that purpose. Tock OS, similar to Zephyr, builds hardware support into the system. The Bern RTOS is also set out to be fail-safe, however, some safety features (some drivers will run in kernel mode) are traded for real-time performance.

In summary the Bern RTOS draft fits somewhere between Drone OS and Tock OS.