% !TEX root = ../Documentation_Bern_RTOS_PAI.tex

\chapter{Introduction}
\label{chap:introduction}

\section{Starting Point}

A few kilobytes of RAM and non-volatile storage of at most 1 MB are prominent restrictions when working with a microcontroller on an embedded system. In addition, the clock most certainly is below 200 MHz, which means the firmware must be efficient. Predominantly C or sometimes C++ is used as the programming language of choice. Another implication of embedded systems is reliability. An electronic locking system runs for years without a reset and still has to perform its task without failure. The firmware therefore must be deterministic and memory safe. This is where C passes the responsibility to the developer. Void pointer casting, pointer management and memory freeing are error prone, leading to \emph{runtime errors}. These errors are expensive to find and fix.

The solution for many of these problems might lie in a fairly new programming language. Rust is a system programming language focused on memory-safety and thread-safety at \emph{compile time} \cite{rust:lang}. Removing the chance for data races and a strong type system help to reduce runtime errors. Because Rust enforces its rules at compile time no overhead is added at runtime, which results in the same performance as code written in C/C++. Thread-safety might seem irrelevant on a single core microcontroller, but comes in use whenever an interrupt service routine is called.

With increasing complexity of an embedded system application it becomes harder to manage all tasks and resources. A real-time operating system (RTOS) helps to organize and synchronize them. It also allows scheduling tasks to a specific time in the future deterministically. As the number of native Rust RTOS is fairly limited, there is an opportunity to develop new Rust RTOS with a new concept.


\section{Objectives}

The main objective of the Bern RTOS project is to write a \emph{fail-safe} RTOS from scratch. As many software bugs as possible should be caught at compile time to increase system stability and to avoid frustrating debugging sessions. Rust promises to achieve this goal, but we will have to figure out how to use the language effectively. A compiler cannot catch every error, therefore, we will need protection at runtime to keep a bug from crashing the whole system.

Developing an RTOS and learning Rust is a big challenge and will take up the majority of my Master of Science in Engineering (MSE) study. The development is taking place over two years and is split into three parts: a first project (9 ECTS), a second project (15 ECTS) and a master thesis (30 ECTS).

The goal of this first project is to create a concept for the Bern RTOS. To gain the necessary background to write a concept, we first need to look at existing RTOS and Rusts concepts. This leads to the following three objectives:
\begin{enumerate}
    \item Existing RTOS should be classified to see what applications these systems target and what features a user expects from an RTOS. In addition, the internal mechanism and structure of an RTOS should be analyzed to identify proven methods that can also be applied in the Bern RTOS.
    
    \item An introduction to Rust should show the concepts and syntax that will help to make the Bern RTOS safe. The focus is to be placed on embedded system applications. The concurrency features from the Rust standard library should be analyzed as well, to see what solutions the language already has for computer systems.
    
    \item Based on the findings of the RTOS analysis, the introduction into Rust and fail-safety in mind, the aim is to work out a concept for Bern RTOS.
\end{enumerate}


\section{Personal Motivation}

I have been programming firmware for microcontrollers with C or modern C++ bare-metal or using FreeRTOS. For example a few colleagues and I used FreeRTOS on an omni wheeled robot to control three motors from a speed vector, keeping track of its position and speed, displaying orientation on an LED strip and simultaneously interfacing a virtual COM port over USB. The RTOS allowed for easy synchronization and communication between the tasks. On the other hand some obscure issues emerged, some from casting a void pointer into the wrong type, sharing the address of variable that later went out of scope and stack overflows. Here I thought that there should be an RTOS that checks the types of messages and tasks parameters at compile time. And that in particular a stack overflow should not crash the system.

In contrast to C, C++ made development more convenient with classes, lists, iterators and generic code through templates. On the other hand, I have to carefully listen to the static analyzer and think of the implication on embedded devices from some features (e.g. virtual methods, polymorphism, RTTI, exceptions).  This is why Rust intrigues me: the language offers modern paradigms, but I do not have to worry about overhead in the assembly output. The drawback of Rust is that the language is still under development, so some feature might just be missing altogether.


\section{Required Knowledge}

It is assumed that the reader has:
\begin{itemize}[nosep]
    \item programming experience
    \item knowledge of embedded systems (microcontrollers)
    \item basic knowledge of RTOS
\end{itemize}