% !TEX root = ../Documentation_Bern_RTOS_PAI.tex

\chapter{RTOS Analysis}
\label{chap:literature-review}

Before we can develop a concept the Bern RTOS, we will look at some RTOS on the market. This gives a general impression on commonly used features a programmer expects. Additionally, we can benefit from already proven techniques and architectures by consulting literature on RTOS implementations.

%-------------------------------------------------------------------------------
\section{Definitions}

In order to compare the different real-time operating systems we first need to define a few terms.

\subsection{Real-Time Operating System}

There is no exact definition of RTOS as \emph{real-time} is dependent on the application. In robotics for example real-time could mean a reaction time of a few tens of milliseconds, while on a solid state disk (SSD) the controller is expected to react in a few microseconds.
Real-time is commonly distinguished between hard and soft. In \emph{hard} real-time a result must be delivered before a deadline, or it will have catastrophic effect on the system \cite[8]{buttazzo_2005}. In \emph{soft} real-time a timely result is expected, a late result however is tolerable.
Although a task on a soft RTOS might run late, it is still expected to run in \emph{deterministic} manner in contrast to a general purpose OS like GNU/Linux or Windows. 

An \emph{operating system} consists of a kernel and modules for hardware access, file access, networking and others. Whether some of these modules run inside or outside the kernel is dependent on the kernel architecture. Most general purpose operating system use a monolithic kernel (GNU/Linux, Windows, macOS), with many modules running inside the kernel. An OS where drivers, file systems and other services are pushed to the user mode is called microkernel. \cite[~65]{tanenbaum_bos_2015}


\subsection{Scheduling}

The main purpose of a scheduler is to share the CPU fairly between multiple tasks that are ready to run \cite[~149]{tanenbaum_bos_2015}. Scheduling adds overhead because the CPU context has to be switched from one task to another. Still, a scheduler can optimize CPU load when one task is waiting for a resource by switching to a ready task.

\begin{figure}[H]
    \centering
    \subfile{images/sched-coop.tex}
    \caption{Cooperative (non-preemptive) scheduling}
    \label{fig:coop}
\end{figure}

Tasks are called \emph{cooperative} or \emph{non-preemptive} if they cannot interrupt (preempt) each other. There can only be one priority for all tasks because a task has to give up the CPU by itself. When a task has finished it yields and calls the scheduler as illustrated in \autoref{fig:coop}. This type of scheduling has minimal overhead as the tasks can run from a single stack and context switches only when absolutely necessary. Nevertheless, the programmer has to ensure a task yields otherwise blocking all the other tasks.

\begin{figure}[H]
    \centering
    \subfile{images/sched-preempt.tex}
    \caption{Priority based preemptive scheduling}
    \label{fig:preempt}
\end{figure}

With the \emph{preemptive} scheduling method tasks can be interrupted before they have finished. If priorities are supported, the task with the highest priority will run and preempt the others. \autoref{fig:preempt} shows an example where an interrupt \circled{1} updates a resource task T2 has been waiting for. Because T2 has the highest priority, is T1 preempted \circled{2}. Only after T2 finished and yields is T1 resumed \circled{3}.

Preemption also applies for tasks of the same priority when a scheduler implements \emph{time slicing} or \emph{round-robin}. In that case the scheduler is periodically called after a defined amount of time. The scheduler will not wait for a task to finish but rather just switch from one task to another with the same priority.

Priority based preemption does also occur on bare-metal systems when a hardware interrupt is triggered. At that point the current program is preempted and the interrupt service routine is called.


\subsection{Inter-Process Communication (IPC)}

Processes on PCs are isolated in their virtual address space. Threads on the other hand run within a process and share an address space. Processes cannot directly communicate. The operating system is responsible to pass on messages and to provide synchronization methods for shared resources. This is called inter-process communication (IPC). 
\cite[~119]{tanenbaum_bos_2015}

Microcontrollers do not have memory management unit (MMU), which is needed for virtual address spaces, resulting in one continuous address space. Thus, only threads/tasks instead of processes are available. But passing information and synchronization is also needed between tasks in the way it used between processes.

As soon as multiple task run in parallel or pseudoparallel access to commonly used resources must be synchronized to avoid race conditions. These accesses can be protected using mutual exclusion (mutex) or semaphores, depending on the resource. Mutex and semaphores are provided by the operating system via system calls.


%-------------------------------------------------------------------------------
\section{RTOS Survey}

The survey consists of a compilation of features and characteristics of readily available RTOS. This part of the analysis does only scratch the surface and its purpose is to get an overview of the landscape of RTOS.

%...............................................................................
\subsection{Characterization}

During the research of different RTOS information regarding the following characteristics was collected.

\subsubsection{Scheduling Algorithm}

There are different scheduling algorithms for operating systems for different use cases. The survey should reveal the most commonly used algorithms.

\subsubsection{Ecosystem}

An ecosystem of an RTOS consists of a kernel, IPC, modules for hardware access, file access and networking and build tools. The ecosystem has major influence on the workflow of a software developer. On an RTOS with lots of tools and custom hardware access layer (HAL) a developer is restricted to the OS specific system calls, on a slim RTOS the developer has more control over the software implementation.

\subsubsection{Documentation \& License}

A thorough documentation is needed to analyze an RTOS. For a detailed insight into RTOS internals such as scheduler and IPC, it is sometimes necessary to read the source code. Consequently, RTOS with an open-source license is preferred for the analysis.

\subsubsection{Functional Safety}

Hard real-time systems are used in safety critical environments. To sell a device for a safety critical application the IEC61508 standard must be considered. IEC61508 SIL-3 will be used to check whether an RTOS meets the functional safety requirements.

%...............................................................................
\subsection{Results}

% manually move to work
\input{tables/rtos-comparison.tex}

\autoref{tab:rtos} shows a vast landscape of different types of RTOS from hardware manufacturers, software companies and open-source communities. They differ in target application, safety and ecosystem completeness. The table does not contain IPC features, as all operating systems include them.


\subsubsection{Groups}

Looking at scheduler types, target application and hardware access we can classify the RTOS into two groups. The group affiliation is shown in the last column and is in most cases similar to target application for which an RTOS is marketed. The two groups that emerge are:
\begin{description}
    \item[Group A: IoT centric] these RTOS were designed or at least marketed for Internet of Things (IoT) devices and hence include networking stacks as well as connectivity drivers. Most of the RTOS provide built-in network drivers for WiFi, BLE and LoRa commonly used in IoT devices. 
    
    \item[Group B: general purpose] the RTOS from this group are used in commercial, industrial, medical and safety critical system.
    Networking stacks are tailored towards industrial use with TCP/IP as default and no built-in BLE support.
\end{description}

The two groups blend into each other, as some RTOS are in the transition from a general purpose to an IoT centric RTOS. Or some target the IoT market even though they can be used for any application.

The prospect of large IoT market seems to wake the interest of big software companies and hardware manufacturers. Amazon bought FreeRTOS and added AWS support, Microsoft bought ThreadX and added Azure Cloud support. Facebook, Google, Intel and Nordic Semiconductor support Zephyr. Texas Instruments develops TI-RTOS, ARM develops Mbed OS based on Keils RTX and Silicon Labs bought \ucos.


\subsubsection{Scheduling}

Preemptive round-robin scheduling seems to be default technique. Contiki-NG chooses to solely schedule cooperatively. Other RTOS offer cooperative scheduling as an alternative to preemptive scheduling for the developer to choose. Zephyr in contrast to all the others offers more involved algorithms such as earliest deadline first, tasklet and multiple task queuing strategies.

\subsubsection{Ecosystem}

The general purpose RTOS generally provide a smaller ecosystem the ones from the IoT group. This might be due to fact, that it is easier to certify and sell individual components instead of one complex system. The general purpose RTOS are also easier to integrate into an existing development workflow. RTOS with a large ecosystem like ARM Mbed OS and Zephyr on the other hand dictate how software is developed.

The RTOS in the IoT group provide their own implementation and API for hardware access (OS Specific HAL in the table) similar to a desktop OS e.g. GNU/Linux. Development on already supported hardware is thus fast. But implementing new driver implies a high initial cost because a developer has to adapt to a new workflow and existing code cannot always be ported to RTOS with its own hardware access API.
The general purpose RTOS provide an API for multitasking and IPC but leave the rest up to the developer. Additional features (file access, network stacks) are available as separate modules. 

In the general purpose group connectivity drivers are rarely included in the RTOS. That is consistent with hardware access philosophy in which the developer has to provide the HAL. In contrast, the operating systems in IoT group provide multiple connectivity drivers, but generally lack drivers for industrial buses (e.g. CAN, Modbus).

Besides the differences in hardware access and networking, most RTOS supply a file system as well as TCP/IP network stack. So the user of an RTOS can expect to get a basic network stack and a basic file system.

Regarding the development workflow Mynewt, ARM Mbed OS, Contiki-NG and Zephyr Project go as far as providing their own build system or integrated development environment (IDE).


\subsubsection{Functional Safety}

All RTOS that have a SIL-3 certification belong to general purpose group. The certified RTOS are variants and none of them are open-source, FreeRTOS (SafeRTOS) \cite{wittenstein:safertos} and \ucos-II \cite{micrium:cert-pack} are selling compatible versions through third-party companies.

Intel is working on the SIL-3 certification of Zephyr \cite{zephyr:functional-safety}. This would make Zephyr the first freely available safety certified RTOS.


\subsubsection{Documentation}

Most RTOS have an open-source codebase allowing for an examination of the specific implementation. Though, not all RTOS can be used freely, e.g. Azure RTOS code is on GitHub but can only be used for free if the hardware is pre-certified (e.g. STM32 MCUs and MPUs). Besides the source code all open-source operating systems also provide an online documentation. 


\subsubsection{Rust}

Separated at the bottom of the table are the RTOS written in the Rust language. These operating systems are newer and do not offer as many features as the more established C/C++ RTOS. But they are going new ways in terms of implementation, RTIC for example is a real-time framework that assigns user code to the interrupt vector table of the microcontroller offloading task switching to hardware. With this multitasking technique there is only one stack, resolving critical stack overflow problems arising from many parallel tasks. 

The Rust RTOS will not be examined in detail in this part but will be consulted for the realization at a later stage.

%-------------------------------------------------------------------------------
\section{Analysis of \ucos-III \& Zephyr Project}

For a more in-depth analysis we select an established RTOS from each group. 

In the IoT group the \emph{Zephyr Project} stands out as feature-rich and rapidly evolving RTOS. Supported by the Linux Foundation, Google, Intel, NXP, Nordic Semiconductor, Facebook and many others the further development is certain.
Zephyr is also of interest because its complex ecosystem, this gives insight on how to design a flexible architecture for a modular system. Due to the large community behind the Zephyr Project there many talks about certain parts of the system, as well as a thorough documentation available online.

\emph{\ucos-III} is selected as a second RTOS for the analysis because it has similar features as the other open-source RTOS from general purpose group, but it is also known for its extensive documentation from its author J. J. Labrosse.

%...............................................................................
\subsection{\ucos-III}

The \ucos-III book \cite{labrosse_2009} explains the design and implementation of a preemptive round-robin scheduler, in addition IPC and memory management are also covered. It goes as far as specific list implementations for different task states. In this subsection I will cover some aspects of the implementation without retyping the book completely.

\subsubsection{Task}

In \ucos-III a task consists of a piece of code that can be called from the scheduler, a stack and a task control block (TCB). The TCB contains all the information about a task a kernel needs for scheduling. Most importantly the priority, a function pointer to the task entry point, a name for debugging, the stack pointer and the state is stored in the  TCB. The state is updated by the kernel according to the state-machine in \autoref{fig:ucos-task-sm}, it governs which tasks are put into ready list. Upon most transitions to/from the Running state a context switch occurs.
\cite[~41]{labrosse_2009}

\begin{figure}[H]
    \centering
    \subfile{images/ucos-task-sm.tex}
    \caption{Task states in \ucos-III (simplified); bold lines mark context switches}
    \label{fig:ucos-task-sm}
\end{figure}


\subsubsection{Scheduling}

\ucos-III schedules tasks preemptively with round-robin. In preemptive scheduling a task is put on hold as soon a higher priority task reaches ready state, e.g. an interrupt routine adds a message to a queue a high priority task is waiting for. To ensure low latency a context switch occurs immediately and a lower priority tasks is preempted, as seen before in \autoref{fig:preempt}.

\begin{figure}[H]
    \centering
    \subfile{images/sched-time-sliced.tex}
    \caption{Round-robin scheduling / time slicing}
    \label{fig:round-robin}
\end{figure}

It is different for multiple tasks of the same priority: one task cannot preempt another. With \emph{round-robin} or \emph{time slicing} the CPU is shared between these tasks equally. After a certain amount of CPU cycles (time quanta), the scheduler is called \circled{1} and another task from the ready list is put into running state. If a task finishes before it used it time quanta it can yield its time \circled{2} and call the scheduler. This implies that the time quanta is the maximal time a task is in running state, it can be shorter though \circled{3}. \cite[~103]{labrosse_2009}


\subsubsection{Context Switching}

A context switch occurs every time another task put into running state. At the beginning of a context switch the kernel pushes CPU registers onto the task stack and stores the stack pointer in the TCB. It then proceeds to load the stack pointer and registers from the next task. \cite[~111]{labrosse_2009}

Minimizing the overhead of context switch is critical, because it at least is called after every time quanta and on interrupts. 


\subsubsection{Critical Sections}

When scheduling tasks or switching context no preemption from a task or interrupt is allowed to take place as it would corrupt the system. These are examples for critical sections. In that case they are protected by disabling all interrupts. Not only uses the scheduler critical sections, they are also needed to access synchronization objects. \cite[~35]{labrosse_2009}

Critical sections should only be used if really necessary and only for short code blocks, because they increase the reaction time an RTOS. Long sections can lead to missing deadlines.

\subsubsection{Software Timer}

For a simple action it is not always necessary to spawn a new task. Periodic or one-shot actions can be run as \emph{callbacks} from a timer task. One single timer task can hold many callback functions. These actions are run from the same stack with little overhead. \cite[~145]{labrosse_2009}

\subsubsection{Interrupt Service}

Interrupt Service Routines (ISR) in \ucos-III that the user writes have to use the assembly language. A message or synchronization primitive can be sent in one of two ways:

\begin{description}
    \item[direct post] after completion of the ISR the interrupted task or newly ready high priority task is called directly.
    
    \item[deferred post] the ISR posts its message to an interrupt queue. The kernel then calls an ISR handler task. After the interrupt queue has been emptied, the system goes back to normal operation.
\end{description}

\subsubsection{Inter-Process Communication}

\ucos-III implements 3 different synchronization primitives and message queues: \cite[~171]{labrosse_2009}
\begin{description}
    \item[semaphore] is used to synchronize tasks or restrict access to a resource. In \ucos-III all semaphores are implemented as \emph{counting semaphores}. They can be used for resources that can be accessed multiple times. When creating a semaphore, the user chooses how many times the semaphore can be taken. A semaphore with a max count of one is called a \emph{binary semaphore}. A semaphore can only be taken, if the internal count is greater than zero. In \ucos-III semaphores cannot prevent an unbounded priority inversion.
     
    \item[mutex] is a binary semaphore, but the difference is that in \ucos-III a mutex features priority inheritance (the preempted task inherits the priority from the high priority task waiting for the mutex), which means that an unbounded priority inversion cannot occur. This mechanism makes a mutex slightly slower than a binary semaphore.
     
    \item[flag groups] are used when a task pends on many events. A task can pend until all events flags are set (conjunctive synchronization) or until one of the event flags is set (disjunctive synchronization).
    
    \item[message queues] are used to pass information from one task or ISR to another task. A message queue is set up with a finite size and in FIFO or LIFO manner. Every queue keeps a list of pending tasks, sorted by priority. When a new message arrives, it is consumed by the highest priority pending task. Alternatively a message can be sent as broadcast to all pending tasks.
    
    To transfer messages efficiently the kernel only sends the pointer to the data. It is up to the user to keep the data in scope, a typical pitfall when using message queues. As a solution \ucos-III suggests using its memory manager, to put the data on the heap.
\end{description}


\subsubsection{Memory Management (Heap)}

Dynamic memory allocation and freeing is prone to fragmentation, because embedded systems run for years without a reset. \ucos-III provides a memory management system that supplies equal sized blocks of memory in partitions, resulting in the layout in \autoref{fig:mem-pool}. The user can set the size of the blocks for each partition at runtime. Memory allocation is now constrained to a fixed size, thus preventing fragmentation.
\cite[~279]{labrosse_2009}

\begin{figure}[H]
    \centering
    \subfile{images/ucos-mempool.tex}
    \caption{Memory partitions with fixed block sizes}
    \label{fig:mem-pool}
\end{figure}

\subsubsection{Memory Safety Mechanism}

In \ucos-III the stack size of a task must be set by the user which typically in done as a rough estimate. Because lots of small stacks are used instead of a large one, stack overflows are a common issue. An overflow can be detected in software or hardware. One software solution is to check that the stack pointer is within the limits set in the TCB. Some CPUs have a stack overflow detection that can be used. Most ARM Cortex-M CPUs feature a Memory Protection Unit (MPU) that can detect access outside set memory regions. 
\cite[~50]{labrosse_2009}

There are other RTOS that use a single stack to minimize the chance of stack overflow to one. A single stack also maximizes memory efficiency because no space is \emph{wasted} on an unused task stack. With a dedicated task stack on the other hand an MPU can rule out manipulation from any other task.

\subsubsection{Source Code}

As of mid 2020 the source code of \ucos-III and all additional components, as well as the full documentation have been published on GitHub \cite{silabs:micrium-github}.

%...............................................................................
\subsection{Zephyr Project}

The review of Zephyr is limited to its architecture and ecosystem to avoid redundancies with the \ucos-III analysis.

The following information is based on the latest version (v2.4.99) of the Zephyr online documentation \cite{linux:zephyr}.

\subsubsection{Architecture}

\begin{figure}[H]
    \includegraphics[width=\linewidth]{zephyr-arch.eps}
    \caption[Zephyr System Architecture]{Zephyr System Architecture (v.2.3.0) \cite{linux:zephyr}}
    \label{fig:zephyr-arch}
\end{figure}

The Zephyr project is structured in layers. The base consists of the hardware platform, either on-chip (SoC peripherals, CPU core) or off-chip (sensors on the board). The description of the platform mainly includes addresses of the memory mapped registers. The scheduler and kernel services access the CPU core on the platform through a first abstraction: the architecture interface. These three layers make up the \emph{kernel}.

Based on the kernel are the \emph{OS services}. These are low level drivers for the hardware interfaces (\iic, SPI, UART) and basic services (file system, IPC), which are abstracted by the low level API. The OS Services also consist of 3 layers of the ISO/OSI model: the data link layer (BLE, IEEE 802.15.4, WiFi), the network layer (IPv6/IPv4) and the transport layer (TCP/UDP).

The session, presentation (TLS) and application (HTTP, MQTT) layer of the ISO/OSI model are put in the \emph{application services}. The services can be accessed through the high level API. The available OS and application services show Zephyrs focus on networking and IoT applications.

The user application can therefore rely on the many features that are already implemented in the Zephyr project. The application itself is completely platform agnostic, as the hardware is abstracted and handled within Zephyr.

The structure of the architecture much resembles a general purpose OS such as GNU/Linux. Thus, managing all the components of Zephyr can be a complex task. To simply this task \emph{Kconfig} is used to configure the components and the \emph{Devicetree} is used to describe the hardware.


\subsubsection{Devicetree}

The Devicetree is data structure that serves as source for all hardware information. In addition to the hardware description the Devicetree also contains its boot-time configuration. The concept of the Devicetree in Zephyr is the same as in Linux, but the implementation and usage are vastly different.

The Devicetree for a board will contain many hierarchically structured files describing the individual peripherals of an SoC (interfaces, GPIOs, ADCs) as well as assignments of board level components to those interfaces. As an example we will take a top-down approach for STM32 Nucleo-F446RE (ARM Cortex-M4F) evaluation board:
    
\begin{customlisting}
\listingpath{zephyr/boards/arm/nucleo\_f446re/nucleo\_f446re.dts}
\begin{minted}[escapeinside=||]{c}
/dts-v1/;
#include <st/f4/stm32f446Xe.dtsi>|\circled{1}|
/*...*/
/ {
    model = "STMicroelectronics STM32F446RE-NUCLEO board";
    compatible = "st,stm32f446re-nucleo";

    chosen {|\circled{2}|
        zephyr,console = &usart2;
        zephyr,shell-uart = &usart2;
        zephyr,sram = &sram0;
        zephyr,flash = &flash0;
    };

    leds {
        compatible = "gpio-leds";
        green_led_2: led_2 {|\circled{3}|
            gpios = <&gpioa 5 GPIO_ACTIVE_HIGH>;
            label = "User LD2";
        };
    };
    /*...*/
};

&usart1 {|\circled{4}|
    pinctrl-0 = <&usart1_tx_pb6 &usart1_rx_pb7>;
    current-speed = <115200>;
    status = "okay";
};
/*...*/
\end{minted}

\caption[Devicetree extract for the STM32 Nucleo-F446RE board]{Devicetree extract for the STM32 Nucleo-F446RE board \cite{zephyr:repo}}
\label{lst:dts-board}
\end{customlisting}

An extract from the Devicetree of the STM32 Nucleo-F446RE board configuration is shown in \autoref{lst:dts-board}. At \circled{1} the data structure of the microcontroller unit (MCU) is included (see \autoref{lst:dts-mcu}). The board Devicetree is one abstraction level above the MCU and only contains peripheral configurations, as the onboard LED at GPIOA5 \circled{3} or the UART port \circled{4} connected to the virtual com port on the debugger. At \circled{2} Zepyhr internals are mapped to specific hardware.

To find the definition of \texttt{usart1} \circled{4} we have to follow the include at \circled{1}.

\begin{customlisting}
\listingpath{zephyr/dts/arm/st/f4/stm32f446.dtsi}
\begin{minted}[escapeinside=||]{c}
#include <st/f4/stm32f401.dtsi>|\circled{1}|
/ {
    soc {
        usart3: serial|@|40004800 {|\circled{2}|
            compatible = "st,stm32-usart", "st,stm32-uart";
            reg = <0x40004800 0x400>;
            clocks = <&rcc STM32_CLOCK_BUS_APB1 0x00040000>;
            interrupts = <39 0>;
            status = "disabled";
            label = "UART_3";
        };
    /*...*/
\end{minted}

\vspace{-1.5\baselineskip}
\listingpath{zephyr/dts/arm/st/f4/stm32f401.dtsi}
\begin{minted}[escapeinside=||]{c}
#include <st/f4/stm32f4.dtsi>|\circled{3}|
/ {
    soc {
        /*...*/|\circled{4}|
\end{minted}

\vspace{-1.5\baselineskip}
\listingpath{zephyr/dts/arm/st/f4/stm32f4.dtsi}
\begin{minted}[escapeinside=||]{c}
#include <arm/armv7-m.dtsi>|\circled{5}|
/*...*/
/ {
    /*...*/
    soc {
        usart1: serial|@|40011000 {|\circled{6}|
            compatible = "st,stm32-usart", "st,stm32-uart";
            reg = <0x40011000 0x400>;
            clocks = <&rcc STM32_CLOCK_BUS_APB2 0x00000010>;
            interrupts = <37 0>;
            status = "disabled";
            label = "UART_1";
        };
    /*...*/
\end{minted}
\caption[Devicetree extracts for the STM32F446RE MCU]{Devicetree extracts for the STM32F446RE MCU \cite{zephyr:repo}}
\label{lst:dts-mcu}
\end{customlisting}

In a microcontroller family most peripherals are shared throughout the different products. Thus, the majority of the hardware structure of the STM32\emph{F446} is included from STM32\emph{F401}, as illustrated at \circled{1} in \autoref{lst:dts-mcu}. Peripherals that are specific to the STM32F446 are configured at \circled{2}.

The STM32\emph{F401} Devicetree source again defines its specific hardware \circled{4} and includes the microcontroller family \circled{3}.

Because \texttt{usart1} is available on all products of the STM32F4 family, it is defined in the STM32F4 device tree file \circled{6}. The extract shows that a generic \texttt{serial} driver is used. The special function register address and the clock source are also given. The data structures regarding the ARM Cortex-M4F CPU then again are defined in another file included at \circled{5}.

From this example we can conclude that the Devicetree splits the hardware into a large hierarchical structure of files, which increases complexity. On the other hand code redundancies are removed, making the adoption of other hardware platforms easier than with manual hardware configuration in the code base. 



\subsubsection{Kconfig}

Kconfig is a configuration tool for the Zephyr kernel and subsystem. The configuration is applied at build time in the manner Kconfig is used for the Linux kernel. Kconfig can be called from the command line as shows in \autoref{fig:kconfig}. Software support such as network stacks, device drivers, file system support and logging features are set in Kconfig. But the target board with its SoC configuration is selected beforehand. Assigning specific peripheral interfaces (e.g. SPI3) to a driver is done in the Devicetree.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{zephyr-kconfig.pdf}
    \caption{Kconfig command line UI}
    \label{fig:kconfig}
\end{figure}


\subsubsection{APIs \& Device Driver Model}

The system architecture in \autoref{fig:zephyr-arch} illustrates the many components Zephyr is made of. All those components are accessed through APIs. The online documentation lists all APIs with their stability status. Some examples are: Audio Codec, CAN, DMA, File Systems, GPIO, Kernel Services, Networking, Shell, User Mode and Watchdog. The different APIs serve as a generic base for the different implementations which are based on specific platforms and protocols. We will now look at the most generic API: the \emph{device driver model}.

According to the device driver model, a device consists of a name, a device configuration, driver data and a driver API. Separating API and data allows for multiple instances of one driver within a system.

\begin{customlisting}
\begin{minted}[escapeinside=||]{c}
static const struct i2c_driver_api|\circled{1}| api_funcs = {
    .configure = i2c_stm32_runtime_configure,
    .transfer = i2c_stm32_transfer
}; 
/*...*/
#define STM32_I2C_INIT(name)|\circled{2}|                                  \
/*...*/
static const struct i2c_stm32_config|\circled{3}| i2c_stm32_cfg_|##|name = { \
        .i2c = (I2C_TypeDef *)DT_REG_ADDR(DT_NODELABEL(name)),   \
        STM32_I2C_IRQ_HANDLER_FUNCTION(name)                     \
        .bitrate = DT_PROP(DT_NODELABEL(name), clock_frequency), \
    /*...*/
};                                                               \
/*...*/
static struct i2c_stm32_data|\circled{4}| i2c_stm32_dev_data_|##|name;       \
/*...*/
DEVICE_DT_DEFINE|\circled{5}|(DT_NODELABEL(name), &i2c_stm32_init,         \
            device_pm_control_nop, &i2c_stm32_dev_data_|##|name,   \
            &i2c_stm32_cfg_|##|name,                               \
            POST_KERNEL, CONFIG_KERNEL_INIT_PRIORITY_DEVICE,     \
            &api_funcs);                                         \
/*...*/
\end{minted}

\caption[STM32 \iic Device Driver extract]{STM32 \iic Device Driver extract \cite{zephyr:repo}}
\label{lst:dev-drv}
\end{customlisting}

A simplified extract of the STM32 \iic driver is shown in \autoref{lst:dev-drv}. At \circled{1} the driver uses the generic \mintinline{c}{i2c_driver_api} and assigns the function implementations to the interface. The name, configuration and data is influenced by the device tree, thus the macro \circled{2} is used. The driver specific configuration of register address and bitrate is stated at \circled{3}. \circled{4} defines the driver data structure, which will be initialized at runtime. Finally, a macro at \circled{5} generates a driver instance through the generic Device Driver Model API.


\subsubsection{West}

West is a meta-tool used to manage Zephyr based projects. It is pluggable meaning a lot of convenient features can be or already have been added.
West is used to:

\begin{itemize}
    \item Set up and update Zephyr based projects (\texttt{west init} and \texttt{west update}), the sources of Zephyr are cloned automatically from many git repositories
    
    \item Build an application (\texttt{west build -p auto -b <board> <application directory>}), the build process itself is based on CMake and the Ninja build system
    
    \item Run or debug an application on the target hardware (\texttt{west flash} and \texttt{west debug})

    \item Call the Kconf UI on the command line (\texttt{west build -t menuconfig}) or in a graphical window (\texttt{west build -t guiconfig})
\end{itemize}
