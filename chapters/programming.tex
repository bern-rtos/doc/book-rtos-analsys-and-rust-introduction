% !TEX root = ../Documentation_Bern_RTOS_PAI.tex

\chapter{Rust Introduction}
\label{chap:rust}

As I am new to the Rust programming language, one part of this project is learning the new ideas, concepts and lastly syntax of the language. I would like to use this chance to discuss a few language concepts of Rust that are particularly interesting for an embedded systems engineer and for use in multi threaded applications.

%-------------------------------------------------------------------------------
\section{Language Characteristics}

Rust introduces some new concepts and programming paradigms that we will take at look now.

%...............................................................................
\subsection{Ownership}

Programming languages take vastly different approaches when it comes to dynamic memory management. In Java and C\# the developer does not bother with memory management, because a garbage collector frees unused objects at runtime. In C, however, it is up to the developer to allocate and free memory in the code. It is similar C++ except we now have smart pointers to help. \mintinline{cpp}{unique_ptr<T>} and \mintinline{cpp}{shared_ptr<T>} track access and delete the referenced object as soon as it is not used anymore.

Rust takes an approach that combines the advantages of the two methods described above: the developer does not have to take care of memory management, but freeing memory is resolved at compile time with no overhead at runtime. The solution is \emph{ownership}. For every variable there is only ever one owner. As soon the owner goes out of scope the memory for the variable can be freed. This allows for the compiler to decide when to free a variable with no cost at runtime. \cite[~71]{blandy_orendorff_2017} \cite[~59]{nichols_rust_2019}

If you want to share a variable without changing its owner, you can use a \emph{reference}. A reference \emph{borrows} a variable from its owner. To ensure that a reference is valid, Rust enforces some rules: \cite[~70]{nichols_rust_2019}
\begin{enumerate}
    \item At any time you can have only one mutable reference, that can change the value of the referenced variable \emph{or} multiple immutable references.
    \item A reference cannot outlive the owner of the variable.
\end{enumerate}

\newpage % todo: fix
\begin{customlisting}
\begin{minted}[escapeinside=||]{rust}
let a: &u32;
{
    let b = 42;
    a = &b;            // 'a' borrows 'b'
    println!("{}", a); // would print 42
}
println!("{}", a);
// compiler error because 'b' does not live long enough
\end{minted}
\caption{Reference mutable borrow}
\label{lst:reference-borrow}
\end{customlisting}

The example in \autoref{lst:reference-borrow} shows an example where the reference \mintinline{rust}{a} outlives the owner of \mintinline{rust}{b}. In C this situation could occur if a function returned a pointer to a local variable from its stack. This typically shows an undefined behavior at runtime when the same spot on the stack is reused. The Rust compiler, in contrast, checks for the lifetime of the variable and throws an error.

The implications of Rusts rules on ownership and references go far beyond memory allocation and freeing. They assure save access to variables by checking lifetimes and single mutable access at compile time. This also solves many issues in concurrent programming, because data races are eliminated.

\begin{customlisting}
\begin{minted}[escapeinside=||]{rust}
let sda = gpiob.pb9.into_alternate_af4_open_drain();
let scl = gpiob.pb8.into_alternate_af4_open_drain();
let i2c|\circled{1}|= hal::i2c::I2c::i2c1(dp.I2C1, (scl, sda), KiloHertz(100), clocks);|\circled{1}|
let mut sensor_imu = Lsm6dso::new(i2c|\circled{2}|, SlaveAddr::Alternate).unwrap();
let mut sensor_temp = Stts751::new(i2c|\circled{3}|).unwrap();
// compiler error because 'sensor_imu' already took ownership of 'i2c'
\end{minted}

\caption{Ownership move}
\label{lst:ownership-move}
\end{customlisting}

In the example code in \autoref{lst:ownership-move} the ownership the \mintinline{rust}{i2c} interface is moved from the local scope \circled{1} to the \mintinline{rust}{Lsm6dso::new()} \circled{2} function. Therefore, the \mintinline{rust}{Stts751::new()} \circled{3} cannot take ownership of the \mintinline{rust}{i2c} interface anymore. As a result the compiler throws an error. The ownership of primitive types (e.g. \mintinline{rust}{u8}) is not moved, it will be simply copied.

%...............................................................................
\subsection{Zero Cost Abstraction}

We use abstraction to make software platform independent, reusable, easier understandable and testable. There is typically a trade off between abstraction and costs at runtime or code implementation, e.g. by using an architecture with many layers, we can easily reuse higher level drivers on another hardware platform, but increase runtime for simple tasks that have to go through all the layers to get to the hardware.

A common abstraction is the use of interfaces. They separate software modules neatly, so software can be reused and is testable. In C++ you can use abstract classes to create an interface. There is however a slight overhead at runtime, because the specific implementation of a method must be selected from the \mintinline{cpp}{vtable}. Rust offers interface in the form of \mintinline{rust}{trait}'s. In contrast to C++ the correct method from the implementations of a trait \emph{can} be selected at compile time. This means that we have no overhead at runtime: a zero cost abstraction. In embedded system development this is especially helpful when we use interfaces to make a module testable. There will only be two implementations: one for the hardware and a mock/stub for testing purposes. 

\begin{customlisting}
\begin{minted}[escapeinside=||]{rust}
pub trait Eucledean3D {|\circled{1}|
    fn get_coordinates(&self) -> (i32, i32, i32);
    fn set_position(&mut self, pos: (i32, i32, i32)) -> ();
}

struct Point{x: i32, y: i32, z: i32}
impl Eucledean3D for Point {|\circled{2}|
    fn get_coordinates(&self) -> (i32, i32, i32) { /*...*/}
    fn set_position(&mut self, pos: (i32, i32, i32)) -> () { /*...*/ }
}

fn move_object<T>(eucledean: &mut T)
    where T: Eucledean3D|\circled{3}|
{
    eucledean.set_position((1,0,0,));
}
/*...*/
fn main() {
    let mut p = Point { x: 0, y: 0, z: 0 };
    move_object(&mut p);|\circled{4}|
}
\end{minted}

\caption{Implementing an interface using Rust traits and generics}
\label{lst:trait}
\end{customlisting}

The first few lines \circled{1} of \autoref{lst:trait} declare a public trait. A trait only contains method declarations and in this case it is used as an interface. The middle section \circled{2} shows the implementation of the trait for \texttt{Point}. In the function \mintinline{rust}{move_object()} the parameter has the generic type \texttt{T}. The \texttt{where} directive \circled{3} tells the compiler that the generic type must implement the \texttt{Eucledean3D} trait. Even though the \mintinline{rust}{move_object()} function uses a generic parameter type, it can access the \mintinline{rust}{set_position()} function from \texttt{Point} \circled{4}.

%...............................................................................
\subsection{Error Handling}

When writing a function in C we normally use the return value for error codes and the actual return value is passed via a pointer in a function parameter. Rust offers a clean solution to handle errors: \mintinline{rust}{Result<ReturnValueType, SomeErrorType>}. The \mintinline{rust}{Result} trait combines errors and return value. \cite[~145]{blandy_orendorff_2017}

\begin{customlisting}
\begin{minted}[escapeinside=||]{rust}
impl Lsm6dso {
    pub fn new(i2c: I2C, address: SlaveAddr) -> Result<Self, Error<E>>|\circled{1}| {
        let mut lsm6dso = Lsm6dso {
            i2c,
            address: address.addr(),
        };
        // Abort and return error code.
        if lsm6dso.get_device_id()?|\circled{2}| != DEVICE_ID {
            return Err(Error::WrongAddress);|\circled{3}|
        }
        // This method could return an error: pass it the caller with '?'.
        lsm6dso.set_gyro_datarate(GyroDataRate::Hz_416)?;
        // Init finished with success, pack the return value into Result.
        Ok(lsm6dso)|\circled{4}|
    }
    /*...*/
}
\end{minted}

\caption{Throwing errors}
\label{lst:error-throwing}
\end{customlisting}

From \circled{1} in \autoref{lst:error-throwing} it is evident, that the function \texttt{new()} returns a result containing a trait object (similar to new in an object-oriented language) or an error of type \texttt{E}. To pack the return value or error code we can use library function \texttt{Err()} \circled{3} and \texttt{Ok()} \circled{4}. The listing also contains the simplest form of error handling. The \texttt{?} \circled{2} after a function denotes that errors will not be handled but rather passed to the caller.

\begin{customlisting}
\begin{minted}[escapeinside=||]{rust}
fn main() -> ! {
    // 1. Abort program (panic) and print stack trace.
    let mut sensor_imu = Lsm6dso::new(i2c, SlaveAddr::Alternate).unwind();
    // 2. Print message if error was returned.
    let mut sensor_imu = Lsm6dso::new(i2c, SlaveAddr::Alternate)
                        .expect("Message");
    // 3. More envolved error handling.
    let mut sensor_imu: Lsm6dso; 
    match Lsm6dso::new(i2c, SlaveAddr::Alternate) {
        Ok(sensor) => {
            sensor_imu = sensor;
        }
        Err(err) => {
            // Use fallback sensor, etc.
        }
    } 
    /*...*/
}
\end{minted}

\caption{Handling errors}
\label{lst:error-handling}
\end{customlisting}

Three more possibilities to handle errors are shown in \autoref{lst:error-handling}. The first one, \texttt{unwind()}, should only be used during debugging as it panics and thus aborts the program.

%...............................................................................
\subsection{Testing}

Many mistakes a programmer can make are already caught by the Rust compiler, but it is limited to static analysis. Support to test the behavior of a module is built right into the language. \cite[~207]{nichols_rust_2019}

\begin{customlisting}
\listingpath{[project]/src/lib.rs}
\begin{minted}[escapeinside=||]{rust}
pub fn a_public_function(a: i32, b: i32) -> i32 {
    a - b
}
fn a_private_function(a: i32, b: i32) -> i32 {
    a + b
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn a_test() {
        assert_eq!(8, a_public_function(25, 17));
        assert_eq!(42, a_private_function(25, 17));
    }
}
\end{minted}

\caption{Unit Test and implementation for \texttt{a\_module}}
\label{lst:test-unit}
\end{customlisting}

A \emph{unit test} is commonly put in the source file of the module under test. \autoref{lst:test-unit} shows the simple set-up for a unit test. With \mintinline{rust}{#[cfg(test)]} the Rust compiler only includes the test modules if the option test is enabled during the build. \mintinline{rust}{#[test]} adds a function to the test runner, which is generated automatically.

\begin{customlisting}
\listingpath{[project]/tests/integration\_test.rs}
\begin{minted}[escapeinside=||]{rust}
use a_module;

#[test]
fn test_public_interface() {
    assert_eq!(7, a_module::a_public_function(42, 35));
}
\end{minted}

\caption{Integration Test for \texttt{a\_module}}
\label{lst:test-integration}
\end{customlisting}

There is also support for \emph{integration tests}. They are put in a separate directory in the project root called \texttt{tests/}. Comparing the integration test in \autoref{lst:test-integration} to unit test above, shows that \mintinline{rust}{#[cfg(test)]} is not necessary anymore as the test is separate module. Further, can only the public functions be accessed.

%...............................................................................
\subsection{Cargo}

Cargo is the build system and package manager for Rust. In contrast to C/C++ projects using CMake or Makefiles in Rust we do not need to list the sources. The mandatory project structure (i.e. sources in the \texttt{src/} directory) allow Cargo and the compiler to resolve file dependencies automatically.

Managing dependencies in C/C++ projects for embedded systems has not been great. With git submodules or CMakes \mintinline{cmake}{FetchContent()} command it is possible to avoid manually copying files between projects. Yet managing dependencies with Cargo is much easier. Mainly because the whole community uses the same system. All \emph{crates} (packages) are hosted on \url{crates.io}, code documentation is done in the source file with comments similar to doxygen and are automatically build and hosted. \cite[~161]{blandy_orendorff_2017}

\newpage % todo: fix
\begin{customlisting}
\listingpath{[project]/Cargo.toml}
\vspace{0.1em}
\begin{minted}{toml}
[package]
name = "project_name"
version = "0.1.0"
authors = ["Stefan Lüthi <stefan.luethi@bfh.ch>"]
edition = "2018"

[dependencies]
embedded-hal = "0.2"
panic-itm = "0.4.1"
\end{minted}

\caption{Cargo project file for an executable}
\label{lst:cargo}
\end{customlisting}

A simple Cargo project file for an executable is shown in \autoref{lst:cargo}. The first section contains some information about the project, after that more options such as dependencies with specific versions are listed.

%-------------------------------------------------------------------------------
\section{Rust on an Embedded Device}

Similar to other programming languages development for embedded devices in Rust differs from computers. In Rust, it is mainly the lack of standard library support. This section covers a few highlights from the development process.

%...............................................................................
\subsection{Demo Application}

To get familiar with Rust in a real-world embedded environment I chose to write a sensor fusion program, estimating the orientation from a gyroscope and an accelerometer. The platform in \autoref{fig:embedded-platform} is based on a NUCLEO-F446RE from ST featuring an ARM Cortex-M4F MCU and X-NUCLEO-IKS01A3 sensor shield from ST as well. The shield contains a 3D accelerometer plus gyroscope sensor (LSM6DSO), an additional 3D accelerometer (LIS2MDL), a 3D magnetometer (LIS2DW12), a pressure sensor (LPS22HH), a humidity and temperature sensor (HTS221) and another temperature sensor (STTS751). 

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.8\linewidth]{eval-board.jpg}
    \caption[Embedded platform: MCU evaluation board and sensor shield]{Embedded platform: MCU evaluation board (NUCLEO-F446RE) and sensor shield (X-NUCLEO-IKS01A3)}
    \label{fig:embedded-platform}
    \vspace{0.5\baselineskip} % todo: fix
\end{figure}

The demo application reads the current acceleration and angular velocity over an \iic bus. It then updates the estimate of the orientation using sensor fusion and prints the result to a serial output (UART) in JSON format. That covers the aspects of typical embedded application:
\begin{enumerate}
    \item Configuring internal peripheral, such as clocks, timers and GPIOs
    \item Accessing board peripherals through a bus (\iic, SPI)
    \item String formatting and serial communication (UART)
    \item Cross compilation and debugging set-up
    \item Modular and reusable software development
\end{enumerate}

We also need to somehow check whether the orientation the embedded application outputs makes sense. For that purpose I wrote a Python script, that plots the current 3D orientation and the measurements from the accelerometer and gyroscope. The plots of an example run are shown in \autoref{fig:orientation-viz}.

\begin{figure}[htb!]
    \includegraphics[width=\linewidth,frame]{orientation-viz.png}
    \caption{Orientation visualization with a Python script}
    \label{fig:orientation-viz}
\end{figure}

The Rust source code for the embedded application is available at \cite{luethi:rust-demo} and the Python script for the visualization at \cite{luethi:pyhton-visualization}.

%...............................................................................
\subsection{Embedded-HAL}

All microcontroller manufacturers provide a hardware abstraction layer (HAL) library to access peripherals without dealing with memory mapped registers. The API for the HAL may vary depending on the manufacturer, though there is some conses that most are written in C and for ARM Cortex CPU core they are compliant with the CMSIS interface. In contrast, HALs in Rust are community driven. The API for a HAL is defined in the \texttt{embedded-hal} crate as traits. They are then implemented for the different microcontrollers, like the \texttt{stm32f4xx-hal} crate. This implies two things: \cite{rust-embedded:book}

\begin{enumerate}
    \item The HAL is completely rewritten by the Rust community and might not be as complete as the manufacturers HAL.
     
    \item Because all implementations use one common API, code is portable between all microcontroller manufacturers and architectures. 
\end{enumerate}

As the HAL development is community driven, it will in some cases not have all the features you need. Another drawback is that even the \texttt{embedded-hal} has not reached a stable release yet. The API of a HAL is thus subject to change. Nevertheless, a unified HAL API is compelling, writing platform agnostic device drivers was never easier.

The Rust community has implemented the HAL differently from a typical manufacturer HAL. We will now look at some differences.

\begin{customlisting}
\begin{minted}[escapeinside=||]{rust}
// take MCU peripherals (can only be called once)
let stm32_peripherals = stm32::Peripherals::take()|\circled{1}|.unwrap(); 

// Set system clock to 48MHz
let rcc = stm32_peripherals.RCC.constrain();
let clocks = rcc.cfgr.sysclk(48.mhz()|\circled{2}|).freeze();

// Set-up GPIOs
let gpiob = stm32_peripherals.GPIOB.split();
let sda = gpiob.pb9.into_alternate_af4_open_drain();|\circled{3}|
let scl = gpiob.pb8.into_alternate_af4_open_drain();

// Configure I2C1 with a speed of 100kHz
let i2c = hal::i2c::I2c::i2c1(
    stm32_peripherals.I2C1,
    (scl, sda),
    KiloHertz(100),|\circled{2}|
    clocks|\circled{4}|
);
\end{minted}

\caption{Configuring an \iic interface with the embedded-hal}
\label{lst:peripheral-config}
\end{customlisting}

At \circled{1} in \autoref{lst:peripheral-config} the stark difference between Rusts ownership and C is evident. In C we can access peripherals in a static context from anywhere, in Rust we take the peripherals and that can only be done once. The embedded-hal uses strong typed values as seen at \circled{2} so that compiler can already check for mistakes. Configuring peripherals in a C vendor HAL is done with a configuration struct passed to a function. The embedded-hal tends to use multiple functions instead \circled{3}. The advantage being that only valid configurations are available and checked at compile time. A common pitfall with C vendor HALs are clock trees that the user has to configure. As demonstrated in \circled{4} the clock tree is passed to the HAL, which applies the correct configuration on its own. 

%...............................................................................
\subsection{Generics}

There is no driver for any of the sensors on the sensor shield X-NUCLEO-IKS01A3 on \url{crates.io}. Thus, support for a few sensors was added based on the existing \texttt{lis3dh} crate. Because with the embedded-hal, every peripheral has its own type a driver that uses a peripheral must be generic.

\begin{customlisting}
\listingpath{lsm6dso/src/lib.rs}
\begin{minted}[escapeinside=||]{rust}
use embedded_hal::blocking::i2c::{Write, WriteRead};

pub struct Lsm6dso<I2C>|\circled{1}| {
    i2c: I2C,|\circled{2}|
    address: u8,
}

impl<I2C, E>|\circled{3}| Lsm6dso<I2C>|\circled{4}|
where
    I2C: WriteRead + Write,|\circled{5}|
{
    /// Create a new LSM6DSO driver from the given I2C peripheral.
    pub fn new(i2c: I2C|\circled{6}|, address: SlaveAddr) -> Result<Self, Error<E>> {
        let mut lsm6dso = Lsm6dso {
            i2c,
            address: address.addr(),
        };
        /*...*/
    }
    /*...*/
}
\end{minted}

\caption{Generalization of sensor driver by using generics}
\label{lst:peripheral-generic}
\end{customlisting}

\autoref{lst:peripheral-generic} contains a simplified extract of the LSM6DSO (accelerometer and gyroscope sensor) driver. First off, the data is defined in a struct (\texttt{Lsm6dso}). Similar to C++ template programming, \texttt{<I2C>} \circled{1} holds the generic type of the \iic interface. The generic type is then used to define \texttt{i2c} in the data structure \circled{2}.

The implementation of \texttt{Lsm6dso} again declares a generic type \circled{3}, which is used specify the struct type \circled{4}. The problem with the generic type \texttt{I2C} is that we are missing the methods to write to the bus. The solution is to declare that the specific type for \texttt{I2C} must implement the \texttt{WriteRead} and \texttt{Write} traits from the embedded-hal \circled{5}. The type of \texttt{I2C} is inferred by the compiler by the \texttt{new()} \circled{6} function call.

%...............................................................................
\subsection{Peripheral Sharing}

Most peripherals, such as GPIOs are only accessed by one software module, with clear ownership of the peripheral. Buses such as SPI or \iic are often accessed from multiple device drivers. But Rusts ownership allows only for one owner. This problem has already been solved by Rust community with the \texttt{shared-bus} crate.

\begin{customlisting}
\begin{minted}[escapeinside=||]{rust}
let sensor_bus = shared_bus::BusManagerSimple::new(i2c|\circled{1}|);
let mut sensor_imu = Lsm6dso::new(sensor_bus.acquire_i2c(),|\circled{2}| SlaveAddr::Alternate).unwrap();
let mut sensor_temp = Stts751::new(sensor_bus.acquire_i2c()|\circled{3}|).unwrap();
\end{minted}
\caption{Sharing an \iic interface in Rust}
\label{lst:peripheral-sharing}
\end{customlisting}

\autoref{lst:peripheral-sharing} shows how to share one single \iic bus \circled{1} between two sensor drivers \circled{2} and \circled{3}. The \texttt{shared-bus} crate contains a mutex to ensure access to the bus is thread/interrupt-safe.

%...............................................................................
\subsection{Multitasking with the RTIC Framework}

Real-Time Interrupt-driven Concurrency (RTIC) is a framework that uses interrupt handlers to offload context switches to the hardware. This is an implementation of the Real-Time for the masses (RTFM) \cite{eriksson:rtfm} concept. The Rust implementation also features compile time guaranteed deadlock free execution and minimal scheduling overhead. 

The RTIC framework heavily uses Rust attributes like \mintinline{rust}{#[rtic::app(...)]} to generate code that handles concurrency. As an example, we will look at the adaptation of the demo application to RTIC.

\begin{customlisting}
\begin{minted}[escapeinside=||]{rust}
#[rtic::app|\circled{1}|(device = stm32f4xx_hal::stm32, /*...*/]
const APP: () = {
    struct Resources|\circled{2}| {
        bus_devices: SharedBusResources<BusType>,
        /*...*/
    }

    #[init|\circled{3}|(schedule = [sense_orientation])]
    fn init(cx: init::Context) -> init::LateResources {
        /* Initialize peripherals and objects ...*/
        cx.schedule|\circled{4}|.sense_orientation(cx.start + DELAY_TICKS)).unwrap();
        init::LateResources|\circled{5}| {
            bus_devices: SharedBusResources { sensor_imu, sensor_temp },
            /*...*/
        }
    }

    #[task|\circled{6}|(resources = [bus_devices], schedule = [sense_orientation])]
    fn sense_orientation(mut cx: sense_orientation::Context) {
        let acceleration = cx.resources|\circled{7}|.bus_devices
                                .sensor_imu.accel_norm().unwrap();
        /*...*/
        cx.schedule.sense_orientation(cx.scheduled + DELAY_TICKS)
            .unwrap();
    }
    /*...*/
    extern "C" {
        fn EXTI0();|\circled{8}|
    }
};
\end{minted}

\caption{Reading a sensor value over \iic using RTIC}
\label{lst:rtic-task}
\end{customlisting}

On the first line of \autoref{lst:rtic-task} a tool attribute \circled{1} denotes that an RTIC application follows. We need to specify which microcontroller is used. The device is later accessible through the task context. Resources that can be used from multiple tasks must be put in the \texttt{Resources} struct \circled{2}. 

Code for initialization has its own attribute \circled{3}. Within the attribute we must tell the framework if we want to schedule any tasks, i.e. \mintinline{rust}{sense_orientation()}. The task is scheduled explicitly at \circled{4}. In the last step \circled{5} of the \texttt{init()} function we fill the resource struct defined at \circled{2}.

The \mintinline{rust}{sense_orientation()} function has the task attribute \circled{6} as well as the resources it will use. The schedule attribute is necessary, so the task can reschedule itself and run periodically. Within the task resources are accessed through the context \circled{7}.

\mintinline{rust}{sense_orientation} is a software task as it is not triggered by any hardware interrupt. Still, with RTIC we need to assign a hardware interrupt for the scheduler \circled{8}, that is not used anywhere else in the code. But one scheduler can run multiple software tasks cooperatively.

%-------------------------------------------------------------------------------
\section{Rust Multitasking API on PC}

One of the obstacles when switching to a new RTOS is to get familiar with the API and programming workflow. To flatten that learning curve we can use a similar syntax a programmer is already used to. So, let us look at threads and IPC in Rust for computer systems.

%...............................................................................
\subsection{Threads}

\vspace{\baselineskip}
\begin{customlisting}
\begin{minted}[escapeinside=//]{rust}
use std::thread;

fn process_large_text(text: Arc<TextMap>) ->  {
    /*...*/
} 

fn main() {
    /*...*/
    let handle = thread::spawn/\circled{1}/(move || 
                    process_large_text(text.clone()));/\circled{2}/
    handle.join().unwrap();/\circled{3}/
}
\end{minted}

\caption{Multitasking using \texttt{std::thread}}
\label{lst:thread-spawn}
\end{customlisting}

A new thread is created and executed immediately with the \mintinline{rust}{thread::spawn()} call \circled{1} in \autoref{lst:thread-spawn}. The parameter of \mintinline{rust}{thread::spawn()} is a capture which is similar to lambda function in other languages. This capture simply calls the \mintinline{rust}{process_large_text()} function and passes a reference to the text \circled{2}. In this case \texttt{clone()} only increments the reference counter of the atomic reference counted type (\mintinline{rust}{Arc<>}). At \circled{3} the program waits until the thread exits. If a thread panics, the \mintinline{rust}{join()} function will return an error. \cite[~457]{blandy_orendorff_2017}

%...............................................................................
\subsection{Inter-Process Communication (IPC)}

The Rust standard library offers some support to communicate between threads. There are other solutions from the community as well, i.e. \texttt{tokio}. This subsection covers mutual exclusion (\texttt{Mutex}) and communication channels (\texttt{channel}) from the Multi-producer single-consumer (mpsc) library.

\subsubsection{Channel}

\vspace{\baselineskip}
\begin{customlisting}
\begin{minted}[escapeinside=//]{rust}
use std::thread;
use std::sync::mpsc::channel;

let (tx, rx) = channel();/\circled{1}/

let handle = thread::spawn(move || {
    tx.send(slow_operation()).unwrap();/\circled{2}/
});
let result = rx.recv().unwrap();/\circled{3}/
handle.join().unwrap();
\end{minted}

\caption{Message passing using a channel}
\label{lst:channel}
\end{customlisting}

A channel consists of one or more senders and a single receiver. At \circled{1} in \autoref{lst:channel} a new channel is created. In the capture \circled{2} the result of \mintinline{rust}{slow_operation()} is sent through the channel. The main program receives the result at \circled{3} and the joins the thread. \cite[~470]{blandy_orendorff_2017}


\subsubsection{Mutex}

\vspace{\baselineskip}
\begin{customlisting}
\begin{minted}[escapeinside='']{rust}
use std::sync::Mutex;
use std::thread;

let data = Mutex::new(42i32);'\circled{1}'
let handle = thread::spawn(move || {
    let mut data_value = data.lock().unwrap();'\circled{2}'
    *data_value += 1;'\circled{3}'
    // mutex automatically unlocked when `data_value` goes out of scope
});
handle.join();
\end{minted}

\caption{Mutual Exclusion (mutex)}
\label{lst:mutex}
\end{customlisting}

In comparison to C/C++ a mutex in Rust is bound to a resource. In \autoref{lst:mutex} at \circled{1} a new mutex containing an integer variable is created. To access the data protected by the mutex a thread must first lock the resource \circled{2}. The value can then be modified \circled{3} and the mutex is automatically unlocked as soon as \mintinline{rust}{data_value} goes out of scope. If a thread locks a resource and then panics, the mutex is marked as \emph{poisoned}. When other threads try to lock a poisoned mutex, an error is returned. \cite[~484]{blandy_orendorff_2017}

%-------------------------------------------------------------------------------
\section{Literature Recommendations}

Rust and its community value open-source a great deal, thus there is lots of excellent resources available online. Almost all the packages on \url{crates.io} have an online documentation on \url{Docs.rs}, which covers the API and general information. That documentation is written in the form of comments in the code. More elaborate Rust projects have specific book-like online documentation generated by \emph{mdBook} to share concepts, ideas and information that is not related to one specific package. I would like to recommend some of these mdBooks, as well as a print book.

The best place to start of is the official Rust book \citetitle{nichols_rust_2019} by \citeauthor{nichols_rust_2019} \cite{nichols_rust_2019}. The book covers the concepts of the programming language with many examples. It is available as online documentation or as print. 

\citetitle{blandy_orendorff_2017} by \citeauthor{blandy_orendorff_2017} \cite{blandy_orendorff_2017} is another great resource, especially for those who already know C++, as it makes many comparisons between the languages.

To start out with Rust on microcontrollers I recommend reading the \citetitle{rust-embedded:book} by \citeauthor{rust-embedded:book} \cite{rust-embedded:book}. It covers the additional information needed to compile and run Rust code on bare-metal devices.

