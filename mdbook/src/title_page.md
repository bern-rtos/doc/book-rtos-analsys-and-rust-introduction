# Bern RTOS: RTOS Analysis and Rust Introduction

<img alt="Title Image" style="display:inline" src="img/title-image.svg"/>

## Authors

- Stefan Lüthi

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" rel="dct:type">work</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

## PDF Version

A PDF version of this book can be downloaded [here](https://gitlab.com/bern-rtos/doc/book-rtos-analsys-and-rust-introduction/-/jobs/artifacts/master/raw/RTOS_Analysis_and_Rust_Introduction.pdf?job=latex).
