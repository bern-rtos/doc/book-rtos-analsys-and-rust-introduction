#!/bin/bash
set -e
dir=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P)
cd $dir

python3 latex2md/convert.py latex2md.toml
python3 latex2md/restructure_files.py latex2md.toml
mdbook build
python3 latex2md/fix_circled.py latex2md.toml
