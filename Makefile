MAIN=RTOS_Analysis_and_Rust_Introduction.tex
LATEX=pdflatex -shell-escape

increment:
	$(LATEX) $(MAIN)

bibliography:
	biber $(MAIN:.tex=)

release:
	$(LATEX) $(MAIN)
	biber $(MAIN:.tex=)
	$(LATEX) $(MAIN)

.DEFAULT_GOAL := increment
